package net.nyagosu.minecraftmodexample;

public class ClientProxy extends CommonProxy {

	public void preInit()
	{
		super.preInit();

		//blocks
		net.nyagosu.minecraftmodexample.blocks.b001_simple_block.StartupClient.preInitClient();
		net.nyagosu.minecraftmodexample.blocks.b002_glow_block.StartupClient.preInitClient();
		net.nyagosu.minecraftmodexample.blocks.b003_hardest_block.StartupClient.preInitClient();
		net.nyagosu.minecraftmodexample.blocks.b004_fragile_block.StartupClient.preInitClient();
		net.nyagosu.minecraftmodexample.blocks.b005_damage_block.StartupClient.preInitClient();

		//items
		net.nyagosu.minecraftmodexample.items.i001_simple_item.StartupClient.preInitClient();

		//net.nyagosu.minecraftmodexample.util.u001_add_fuel.StartupClient.preInitClient();
	}

	public void init()
	{
		super.init();

		//blocks
		net.nyagosu.minecraftmodexample.blocks.b001_simple_block.StartupClient.initClient();
		net.nyagosu.minecraftmodexample.blocks.b002_glow_block.StartupClient.initClient();
		net.nyagosu.minecraftmodexample.blocks.b003_hardest_block.StartupClient.initClient();
		net.nyagosu.minecraftmodexample.blocks.b004_fragile_block.StartupClient.initClient();
		net.nyagosu.minecraftmodexample.blocks.b005_damage_block.StartupClient.initClient();

		//items
		net.nyagosu.minecraftmodexample.items.i001_simple_item.StartupClient.initClient();

		//net.nyagosu.minecraftmodexample.util.u001_add_fuel.StartupClient.initClient();
	}

	public void postInit()
	{
		super.postInit();

		//blocks
		net.nyagosu.minecraftmodexample.blocks.b001_simple_block.StartupClient.postInitClient();
		net.nyagosu.minecraftmodexample.blocks.b002_glow_block.StartupClient.postInitClient();
		net.nyagosu.minecraftmodexample.blocks.b003_hardest_block.StartupClient.postInitClient();
		net.nyagosu.minecraftmodexample.blocks.b004_fragile_block.StartupClient.postInitClient();
		net.nyagosu.minecraftmodexample.blocks.b005_damage_block.StartupClient.postInitClient();

		//items
		net.nyagosu.minecraftmodexample.items.i001_simple_item.StartupClient.postInitClient();

		//net.nyagosu.minecraftmodexample.util.u001_add_fuel.StartupClient.postInitClient();
	}

}
