package net.nyagosu.minecraftmodexample;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(
	modid = MinecraftModExample.MODID,
	name = MinecraftModExample.MOD_NAME,
	version = MinecraftModExample.VERSION
)


public class MinecraftModExample {

	public static final String MODID = "minecraftmodexample";
	public static final String MOD_NAME = "MinecraftModExample";
	public static final String VERSION = "1.0.0";

	@Mod.Instance(MinecraftModExample.MODID)
    public static MinecraftModExample instance;

	@SidedProxy(clientSide="net.nyagosu.minecraftmodexample.ClientProxy", serverSide="net.nyagosu.minecraftmodexample.CommonProxy")
    public static CommonProxy proxy;

	@EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
		proxy.preInit();
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	proxy.init();
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	proxy.postInit();
    }

}
