package net.nyagosu.minecraftmodexample.items.i001_simple_item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemSimpleItem extends Item {

	public ItemSimpleItem()
	{
		//this.setMaxStackSize(1);
		this.setCreativeTab(CreativeTabs.MISC);
	}
}
