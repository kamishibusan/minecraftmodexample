package net.nyagosu.minecraftmodexample.items.i001_simple_item;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraftforge.client.model.ModelLoader;

public class StartupClient {

	public static void preInitClient()
	{
		ModelResourceLocation itemModelResourceLocation = new ModelResourceLocation("minecraftmodexample:i001_item_simple_item", "inventory");
	    final int DEFAULT_ITEM_SUBTYPE = 0;
	    ModelLoader.setCustomModelResourceLocation(StartupCommon.itemSimpleItem, DEFAULT_ITEM_SUBTYPE, itemModelResourceLocation);
	}

	public static void initClient()
	{

	}

	public static void postInitClient()
	{

	}
}
