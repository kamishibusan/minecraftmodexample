package net.nyagosu.minecraftmodexample.items.i001_simple_item;

import net.minecraftforge.fml.common.registry.GameRegistry;

public class StartupCommon {

	public static ItemSimpleItem itemSimpleItem;

	public static void preInitCommon()
	{
		itemSimpleItem = (ItemSimpleItem) (new ItemSimpleItem().setUnlocalizedName("i001_item_simple_item"));
		itemSimpleItem.setRegistryName("i001_item_simple_item");
		GameRegistry.register(itemSimpleItem);

	}

	public static void initCommon()
	{

	}

	public static void postInitCommon()
	{

	}
}
