package net.nyagosu.minecraftmodexample;

public class CommonProxy {

	public void preInit()
	{
		//blocks
		net.nyagosu.minecraftmodexample.blocks.b001_simple_block.StartupCommon.preInitCommon();
		net.nyagosu.minecraftmodexample.blocks.b002_glow_block.StartupCommon.preInitCommon();
		net.nyagosu.minecraftmodexample.blocks.b003_hardest_block.StartupCommon.preInitCommon();
		net.nyagosu.minecraftmodexample.blocks.b004_fragile_block.StartupCommon.preInitCommon();
		net.nyagosu.minecraftmodexample.blocks.b005_damage_block.StartupCommon.preInitCommon();

		//items
		net.nyagosu.minecraftmodexample.items.i001_simple_item.StartupCommon.preInitCommon();

		//net.nyagosu.minecraftmodexample.util.u001_add_fuel.StartupCommon.preInitCommon();
	}

	public void init()
	{
		//blocks
		net.nyagosu.minecraftmodexample.blocks.b001_simple_block.StartupCommon.initCommon();
		net.nyagosu.minecraftmodexample.blocks.b002_glow_block.StartupCommon.initCommon();
		net.nyagosu.minecraftmodexample.blocks.b003_hardest_block.StartupCommon.initCommon();
		net.nyagosu.minecraftmodexample.blocks.b004_fragile_block.StartupCommon.initCommon();
		net.nyagosu.minecraftmodexample.blocks.b005_damage_block.StartupCommon.initCommon();

		//items
		net.nyagosu.minecraftmodexample.items.i001_simple_item.StartupCommon.initCommon();

		//net.nyagosu.minecraftmodexample.util.u001_add_fuel.StartupCommon.initCommon();
	}

	public void postInit()
	{
		//blocks
		net.nyagosu.minecraftmodexample.blocks.b001_simple_block.StartupCommon.postInitCommon();
		net.nyagosu.minecraftmodexample.blocks.b002_glow_block.StartupCommon.postInitCommon();
		net.nyagosu.minecraftmodexample.blocks.b003_hardest_block.StartupCommon.postInitCommon();
		net.nyagosu.minecraftmodexample.blocks.b004_fragile_block.StartupCommon.postInitCommon();
		net.nyagosu.minecraftmodexample.blocks.b005_damage_block.StartupCommon.postInitCommon();

		//items
		net.nyagosu.minecraftmodexample.items.i001_simple_item.StartupCommon.postInitCommon();

		//net.nyagosu.minecraftmodexample.util.u001_add_fuel.StartupCommon.postInitCommon();
	}
}
