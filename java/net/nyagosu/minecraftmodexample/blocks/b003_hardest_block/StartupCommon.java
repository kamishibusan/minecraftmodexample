package net.nyagosu.minecraftmodexample.blocks.b003_hardest_block;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class StartupCommon {

	public static BlockHardestBlock blockHardestBlock;
	public static ItemBlock itemBlockHardestBlock;

	public static void preInitCommon()
	{
		blockHardestBlock = (BlockHardestBlock)(new BlockHardestBlock().setUnlocalizedName("b003_block_hardest_block"));
		blockHardestBlock.setRegistryName("b003_block_hardest_block");
		GameRegistry.register(blockHardestBlock);

		itemBlockHardestBlock = new ItemBlock(blockHardestBlock);
		itemBlockHardestBlock.setRegistryName(blockHardestBlock.getRegistryName());
		GameRegistry.register(itemBlockHardestBlock);
	}

	public static void initCommon()
	{

	}

	public static void postInitCommon()
	{

	}
}
