package net.nyagosu.minecraftmodexample.blocks.b003_hardest_block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockHardestBlock extends Block {

	public BlockHardestBlock()
	{
		super(Material.ROCK);
		this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);

		this.setHardness(100.0F);
	}
}
