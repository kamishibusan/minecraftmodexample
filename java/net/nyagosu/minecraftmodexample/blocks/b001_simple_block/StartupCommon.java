package net.nyagosu.minecraftmodexample.blocks.b001_simple_block;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class StartupCommon {
	public static BlockSimple blockSimple;
	public static ItemBlock itemBlockSimple;

	public static void preInitCommon()
	{
		blockSimple = (BlockSimple)(new BlockSimple().setUnlocalizedName("b001_block_simple"));
		blockSimple.setRegistryName("b001_block_simple");
		GameRegistry.register(blockSimple);

		itemBlockSimple = new ItemBlock(blockSimple);
		itemBlockSimple.setRegistryName(blockSimple.getRegistryName());
		GameRegistry.register(itemBlockSimple);
	}

	public static void initCommon()
	{

	}

	public static void postInitCommon()
	{

	}
}
