package net.nyagosu.minecraftmodexample.blocks.b001_simple_block;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraftforge.client.model.ModelLoader;

public class StartupClient {
	public static void preInitClient()
	{
		ModelResourceLocation itemModelResourceLocation = new ModelResourceLocation("minecraftmodexample:b001_block_simple", "inventory");
		final int DEFAULT_ITEM_SUBTYPE = 0;
		ModelLoader.setCustomModelResourceLocation(StartupCommon.itemBlockSimple, DEFAULT_ITEM_SUBTYPE, itemModelResourceLocation);
	}

	public static void initClient()
	{

	}

	public static void postInitClient()
	{

	}
}
