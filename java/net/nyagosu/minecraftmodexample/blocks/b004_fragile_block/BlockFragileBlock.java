package net.nyagosu.minecraftmodexample.blocks.b004_fragile_block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockFragileBlock extends Block {

	public BlockFragileBlock()
	{
		super(Material.ROCK);
		this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);

		/*
		 * 黒曜石の2倍の硬さ 100.0F
		 */

		this.setHardness(0.0F);
	}
}
