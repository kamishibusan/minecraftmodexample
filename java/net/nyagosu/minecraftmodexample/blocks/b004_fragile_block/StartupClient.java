package net.nyagosu.minecraftmodexample.blocks.b004_fragile_block;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraftforge.client.model.ModelLoader;

public class StartupClient {

	public static void preInitClient()
	{
		ModelResourceLocation itemModelResourceLocation = new ModelResourceLocation("minecraftmodexample:b004_block_fragile_block", "inventory");
		final int DEFAULT_ITEM_SUBTYPE = 0;
		ModelLoader.setCustomModelResourceLocation(StartupCommon.itemBlockFragileBlock, DEFAULT_ITEM_SUBTYPE, itemModelResourceLocation);
	}

	public static void initClient()
	{

	}

	public static void postInitClient()
	{

	}
}
