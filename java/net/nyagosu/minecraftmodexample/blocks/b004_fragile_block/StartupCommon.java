package net.nyagosu.minecraftmodexample.blocks.b004_fragile_block;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class StartupCommon {

	public static BlockFragileBlock blockFragileBlock;
	public static ItemBlock itemBlockFragileBlock;

	public static void preInitCommon()
	{
		blockFragileBlock = (BlockFragileBlock)(new BlockFragileBlock().setUnlocalizedName("b004_block_fragile_block"));
		blockFragileBlock.setRegistryName("b004_block_fragile_block");
		GameRegistry.register(blockFragileBlock);

		itemBlockFragileBlock = new ItemBlock(blockFragileBlock);
		itemBlockFragileBlock.setRegistryName(blockFragileBlock.getRegistryName());
		GameRegistry.register(itemBlockFragileBlock);
	}

	public static void initCommon()
	{

	}

	public static void postInitCommon()
	{

	}
}
