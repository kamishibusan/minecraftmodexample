package net.nyagosu.minecraftmodexample.blocks.b002_glow_block;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraftforge.client.model.ModelLoader;

public class StartupClient {
	public static void preInitClient()
	{
		ModelResourceLocation itemModelResourceLocation = new ModelResourceLocation("minecraftmodexample:b002_block_glow_block", "inventory");
		final int DEFAULT_ITEM_SUBTYPE = 0;
		ModelLoader.setCustomModelResourceLocation(StartupCommon.itemBlockGlowBlock, DEFAULT_ITEM_SUBTYPE, itemModelResourceLocation);
	}

	public static void initClient()
	{

	}

	public static void postInitClient()
	{

	}
}
