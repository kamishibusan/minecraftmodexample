package net.nyagosu.minecraftmodexample.blocks.b002_glow_block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockGlowBlock extends Block {

	public BlockGlowBlock()
	{
		super(Material.ROCK);
		this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);

		this.setHardness(3.0F);

		/*
		 * Light Level
		 * 0.0F - 1.0F
		 */

		this.setLightLevel(1.0F);
	}

}
