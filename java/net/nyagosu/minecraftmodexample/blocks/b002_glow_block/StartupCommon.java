package net.nyagosu.minecraftmodexample.blocks.b002_glow_block;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class StartupCommon {

	public static BlockGlowBlock blockGlowBlock;
	public static ItemBlock itemBlockGlowBlock;

	public static void preInitCommon()
	{
		blockGlowBlock = (BlockGlowBlock)(new BlockGlowBlock().setUnlocalizedName("b002_block_glow_block"));
		blockGlowBlock.setRegistryName("b002_block_glow_block");
		GameRegistry.register(blockGlowBlock);

		itemBlockGlowBlock = new ItemBlock(blockGlowBlock);
		itemBlockGlowBlock.setRegistryName(blockGlowBlock.getRegistryName());
		GameRegistry.register(itemBlockGlowBlock);
	}

	public static void initCommon()
	{

	}

	public static void postInitCommon()
	{

	}
}