package net.nyagosu.minecraftmodexample.blocks.b005_damage_block;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class StartupCommon {

	public static BlockDamageBlock blockDamageBlock;
	public static ItemBlock itemBlockDamageBlock;

	public static void preInitCommon()
	{
		blockDamageBlock = (BlockDamageBlock)(new BlockDamageBlock().setUnlocalizedName("b005_block_damage_block"));
		blockDamageBlock.setRegistryName("b005_block_damage_block");
		GameRegistry.register(blockDamageBlock);

		itemBlockDamageBlock = new ItemBlock(blockDamageBlock);
		itemBlockDamageBlock.setRegistryName(blockDamageBlock.getRegistryName());
		GameRegistry.register(itemBlockDamageBlock);
	}

	public static void initCommon()
	{

	}

	public static void postInitCommon()
	{

	}
}
