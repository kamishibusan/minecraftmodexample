package net.nyagosu.minecraftmodexample.util.u001_add_fuel;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.IFuelHandler;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class StartupCommon {

	public static void preInitCommon()
	{
		GameRegistry.registerFuelHandler(new IFuelHandler()
		{
			@Override
			public int getBurnTime(ItemStack fuel)
			{
				if(fuel.getItem().equals(Items.EGG))return 60;
				return 0;
			}
		});
	}

	public static void initCommon()
	{

	}

	public static void postInitCommon()
	{

	}

}
